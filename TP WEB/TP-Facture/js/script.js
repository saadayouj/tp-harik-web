var cte=0;

function clears(){
	document.getElementById('lib').value="";
	document.getElementById('prixun').value="";
	document.getElementById('qte').value="";
}

function printt()
{
	if(cte==0){
		alert('Le Contenu de la facture est vide!');
		return;
	}
	else{
		window.print();
	}
}


function ajouter(){
	var a = document.getElementById('lib').value;
	var b = document.getElementById('prixun').value;
	var c = document.getElementById('qte').value;
	if(test(a,b,c)==true)
		{
			if(cte==0){
				document.getElementById('toptable').innerHTML="<td>Libellé</td><td>Prix Unitaire</td><td>Quantité</td><td>Prix Total</td>";
			}
			var rtht = document.getElementById("tht");
			var tva = document.getElementById("tva").value;
			cte=cte+1;
			var res=parseFloat(b*c);
			var restth=parseFloat(rtht.value)+res;
			document.getElementById("tht").value=restth;
			document.getElementById("ttc").value=restth+(restth*tva/100);
			document.getElementById('donnee').innerHTML+="<tr><td id='three'><input type='text' value='"+a+"' id='vala"+cte+"'/></td><td id='three'><input type='text' value='"+b+"' id='valb"+cte+"'/></td><td id='three'><input type='text' value='"+c+"' id='valc"+cte+"'/></td><td id='three'><input type='text' value='"+res+"' id='vald"+cte+"' disabled/></td><td id='one'><input type='button' value='Modifier' id='edit' onclick='modifier("+cte+")' /></td></tr>";
			document.getElementById('lib').value="";
			document.getElementById('prixun').value="";
			document.getElementById('qte').value="";
		}
}

function modifier(x){
	var d=document.getElementById("vald"+x);
	var tva = document.getElementById("tva").value;

	var result=parseFloat(document.getElementById("valb"+x).value*document.getElementById("valc"+x).value);
	document.getElementById("vald"+x).value=result;
	var result_total=0;
	for(let i=1;i<=cte;i++)
	{
		result_total=parseFloat(result_total)+parseFloat(document.getElementById("vald"+i).value);
	}
	document.getElementById("tht").value=result_total;
	document.getElementById("ttc").value=result_total+(result_total*tva/100);
}

function test(a,b,c)
{
	var d=false;
	if(a=="")
	{
		d=false;
		alert('Veuillez entrer le Libellé ');
		document.getElementById("lib").focus();
	}
	if (isNaN(b)&&b=="") {
		d=false;
		alert('Veuillez entrer le Prix Unitaire');
		document.getElementById("prixun").focus();
	}
	if (isNaN(c) && c=="") {
		d=false;
		alert('Veuillez entrer la Quantité');
		document.getElementById("qte").focus();
	}
	else {
		d=true;
	}
	return d;
}